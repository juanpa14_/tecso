package coop.tecso.examen.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.exception.HolderException;
import coop.tecso.examen.model.Holder;
import coop.tecso.examen.model.enu.HolderType;
import coop.tecso.examen.repository.HolderRepository;
import coop.tecso.examen.service.HolderService;

@Service
public class HolderServiceImpl implements HolderService {
    
    // ---------------
    // Attributes
    // ---------------
    @Autowired
    private HolderRepository repository;
    

    // ---------------
    // Methods
    // ---------------
    @Override
    public Holder saveHolder (Holder holder) throws HolderException
    {
        return repository.save (holder);
    }

    @Override
    public Holder updateHolder (Holder holder, Long id) throws HolderException
    {
        Holder current = findById (id);
        
        if (current == null) {
            throw new HolderException ("El titular no existe");
        }
        
        return repository.save (current);
    }

    @Override
    public void delete (Long id) throws HolderException
    {
        Holder holder = findById (id);
        
        if (holder == null) {
            throw new HolderException ("El titular no existe");
        }
        
        repository.delete (holder);
    }

    @Override
    public Holder getById (Long id) throws HolderException
    {
        Holder holder = findById (id);
        
        if (holder == null) {
            throw new HolderException ("El titular no existe");
        }
        
        return holder;
    }

    @Override
    public List<Holder> getJuridicHolders ()
    {
        return repository.findByType (HolderType.NIT);
    }

    @Override
    public List<Holder> getNatureHolders ()
    {
        return repository.findByType (HolderType.CC);
    }
    
    private Holder findById (Long id) {
        Optional<Holder> holder = repository.findById (id);
        return holder.isPresent () ? holder.get () : null;
    }

}
