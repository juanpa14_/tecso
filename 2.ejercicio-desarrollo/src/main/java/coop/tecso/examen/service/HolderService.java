package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.exception.HolderException;
import coop.tecso.examen.model.Holder;

public interface HolderService
{

    // ---------------
    // Methods
    // ---------------
    Holder saveHolder (Holder holder) throws HolderException;
    
    Holder updateHolder (Holder holder, Long id) throws HolderException;
    
    void delete (Long id) throws HolderException;
    
    Holder getById (Long id) throws HolderException;

    List<Holder> getJuridicHolders ();
    
    List<Holder> getNatureHolders ();
    
}
