package coop.tecso.examen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coop.tecso.examen.model.Holder;
import coop.tecso.examen.model.enu.HolderType;

@Repository
public interface HolderRepository extends JpaRepository<Holder, Long> {

    // ---------------
    // Methods
    // ---------------
    List<Holder> findByType(HolderType type);
    
}