package coop.tecso.examen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import coop.tecso.examen.model.enu.CurrencyType;

@Entity
@Table(name = "ACCOUNT")
public class Account
{

    // ---------------
    // Attributes
    // ---------------
    @Id
    @Column(name = "id", length = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNT_SEQ")
    @SequenceGenerator(sequenceName = "account_seq", allocationSize = 1, name = "account_sec")
    private Long id;

    @Column(name = "number", length = 100, nullable = false)
    private String accountNumber;

    @Column(name = "balance", precision = 2, nullable = false)
    private Double balance;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private CurrencyType currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "holder_id")
    private Holder holder;

    // ---------------
    // Methods
    // ---------------
    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getAccountNumber ()
    {
        return accountNumber;
    }

    public void setAccountNumber (String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public Double getBalance ()
    {
        return balance;
    }

    public void setBalance (Double balance)
    {
        this.balance = balance;
    }

    public CurrencyType getCurrency ()
    {
        return currency;
    }

    public void setCurrency (CurrencyType currency)
    {
        this.currency = currency;
    }

    public Holder getHolder ()
    {
        return holder;
    }

    public void setHolder (Holder holder)
    {
        this.holder = holder;
    }

}
