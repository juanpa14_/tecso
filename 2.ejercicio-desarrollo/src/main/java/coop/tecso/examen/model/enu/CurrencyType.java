package coop.tecso.examen.model.enu;

public enum CurrencyType
{

    // ---------------
    // Values
    // ---------------
    DOLAR(3400),
    EURO(3800),
    PESO(1);

    // ---------------
    // Attributes
    // ---------------
    private int value;

    // ---------------
    // Constructor
    // ---------------
    CurrencyType(int value) {
        this.value = value;
    }

    // ---------------
    // Methods
    // ---------------
    public int getValue() {
        return value;
    }

}
