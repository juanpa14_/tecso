package coop.tecso.examen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import coop.tecso.examen.model.enu.HolderType;

@Entity
@Table(name = "HOLDER")
public class Holder
{
    
    // ---------------
    // Attributes
    // ---------------
    @Id
    @Column(name = "id", length = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HOLDER_SEQ")
    @SequenceGenerator(sequenceName = "holder_seq", allocationSize = 1, name = "holder_sec")
    private Long id;

    
    @Column(name = "description", length = 100, nullable = true)
    private String description;
    
    @Column(name = "fund_date", nullable = true)
    private Date fundDate;

    
    @Column(name = "names", length = 80, nullable = true)
    private String names;
    
    @Column(name = "surname", length = 250, nullable = true)
    private String surname;

    
    @Column(name = "rut", nullable = false)
    private String rut;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private HolderType type;
    
    @OneToMany(mappedBy="holder", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Account> accounts = new ArrayList<>();

    // ---------------
    // Methods
    // ---------------
    public Long getId ()
    {
        return id;
    }
    public void setId (Long id)
    {
        this.id = id;
    }
    public String getDescription ()
    {
        return description;
    }
    public void setDescription (String description)
    {
        this.description = description;
    }
    public Date getFundDate ()
    {
        return fundDate;
    }
    public void setFundDate (Date fundDate)
    {
        this.fundDate = fundDate;
    }
    public String getNames ()
    {
        return names;
    }
    public void setNames (String names)
    {
        this.names = names;
    }
    public String getSurname ()
    {
        return surname;
    }
    public void setSurname (String surname)
    {
        this.surname = surname;
    }
    public String getRut ()
    {
        return rut;
    }
    public void setRut (String rut)
    {
        this.rut = rut;
    }
    public HolderType getType ()
    {
        return type;
    }
    public void setType (HolderType type)
    {
        this.type = type;
    }
    public List<Account> getAccounts ()
    {
        return accounts;
    }
    public void setAccounts (List<Account> accounts)
    {
        this.accounts = accounts;
    }
    
}
