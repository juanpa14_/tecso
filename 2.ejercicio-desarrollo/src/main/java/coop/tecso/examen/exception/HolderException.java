package coop.tecso.examen.exception;

public class HolderException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7636231842168727412L;

    public HolderException(String message) {
        super(message);
    }
    
}
